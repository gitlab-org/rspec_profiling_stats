# GitLab RSpec Profiling Statistics

A simple CI job which generates basic statistics about most
expensive RSpec tests. Identifying slowest / most expensive tests can help with
optimizing our code and our specs.

These statistics are generated every day and published on:

https://gitlab-org.gitlab.io/rspec_profiling_stats/

## What if I found a suspicious test?

If you find a suspicious test which could (or should) be optimized, you can
[create and issue on
gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=&issue%5Bdescription%5D=%0D%0A%2Flabel+~"rspec+profiling"+)
(please add ~"rspec profiling" label to related issues and merge requests to
make it easier to track these optimizations).

## What if there is a missing statistic?

If you know about some useful statistics which could be added, you can create an
[issue](https://gitlab.com/gitlab-org/rspec_profiling_stats/-/issues/new) for
adding it, or send a merge request which adds the query to
`rspec_stats/rspec_queries.rb` file.

**NOTE** `PGURI` variable for connecting to the CI RSpec stats database is
available only on protected branches (`master` and `next`).

**NOTE** There is a plan to make profiling data [available also in
Sisense](https://gitlab.com/gitlab-data/analytics/-/issues/4912) which will make it easier
to run custom queries so we may replace the temporary HTML tables with data
from Sisense.

## Can I download historic data?

Yes, we preserve the generated data (CSV and JSON) for one month. Feel free to
[browse or download the artifacts from the past `pages` job](https://gitlab.com/gitlab-org/rspec_profiling_stats/-/pipelines?page=1&scope=all&ref=master).

## Contributing

### Development

#### Without database setup

If you want to run this project locally to test/improve only the frontend you can
can download the latest CSVs from https://gitlab-org.gitlab.io/rspec_profiling_stats/ (see CSV links)
and save them in `tmp/rspec_stats`. The downloaded CSVs are used to generate the HTML so
you don't need to setup and a seed a database (as described below).

Generate the HTML with:

```sh
ruby rspec_stats/rspec_stats.rb
```

#### With database setup

You can run this project locally using the seed data in `db/seed.sql`. You'll
need to set up a PostgreSQL 11 database with a role named 'honza' and a password
of your choice, then use the SQL dump provided to create the data.

1. Open `psql` as the postgres user: `sudo -u postgres psql`;
1. Create a role called 'honza' including a password of your choice: `CREATE USER honza WITH LOGIN PASSWORD '[password]';`;
1. Create a database called 'ci_stats': `CREATE DATABASE ci_stats;`;
1. Assign all privileges on 'ci_stats' to 'honza': `GRANT ALL ON DATABASE ci_stats TO honza;`;
1. Add the custom [`feature_category` column](https://docs.gitlab.com/ee/development/feature_categorization/#rspec-examples)
if not present `ALTER TABLE spec_profiling_results ADD COLUMN feature_category text;`.
1. Exit `psql` with `\q`;
1. Import all seed data into the new database: `psql postgres://honza:[password]@localhost/ci_stats < db/seed.sql`, and
1. Set the `PGURI` envvar: `export PGURI=postgres://honza:[password]@localhost/ci_stats`.

This only needs to be done the first time you run the project. Once you have
seed data you can regenerate the site any time to preview your changes using 
the following command:

```sh
ruby rspec_stats/rspec_stats.rb
```

This generates a HTML file and support materials in the `tmp` directory.

If you want to regenerate the CSVs please remove them first with:

```sh
rm -fr tmp/rspec/*.csv
ruby rspec_stats/rspec_stats.rb
```

For starting a WEBrick server and check the generated HTML pages, use:

```sh
ruby rspec_stats/server
```

And access in browser http://localhost:9111/

### Production

In production, this database exists in the `gitlab-eng-productivity` project with the name `rspec-profiling`. It is [managed in terraform](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/master/environments/eng-productivity/main.tf).

The schema is unmanaged, schema changes need to be made manually and changes should be reflected back in this repository.

#### Connecting via cloud shell

Setting up user access:

```
$ gcloud --project gitlab-eng-productivity sql users create $USER@gitlab.com --instance rspec-profiling --type CLOUD_IAM_USER
```

And someone with the `cloudsqlsuperuser` role needs to grant access to this user:

```
ci_stats=> grant cloudsqlsuperuser to "$USER@gitlab.com";
```

This setup only needs to be done once, and it should go through an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests).

To connect to the production database:

```shell
# Optional: start cloud-shell, you can also run this locally.
➜  ~ gcloud --project gitlab-eng-productivity cloud-shell ssh --authorize-session

# install and start cloud sql proxy
$ wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O cloud_sql_proxy
$ chmod +x cloud_sql_proxy
$ ./cloud_sql_proxy -enable_iam_login -instances gitlab-eng-productivity:us-east1:rspec-profiling=tcp:5432 &

# connect via psql
$ psql "host=127.0.0.1 sslmode=disable dbname=ci_stats user=$USER@gitlab.com"
```

#### Connecting from M1 macOS workstation

To connect from an M1 mac, the instructions are slightly different:

```shell
# install and start cloud sql proxy
$ wget https://dl.google.com/cloudsql/cloud_sql_proxy.darwin.arm64 -O cloud_sql_proxy
$ chmod +x cloud_sql_proxy
$ ./cloud_sql_proxy -enable_iam_login -instances gitlab-eng-productivity:us-east1:rspec-profiling=tcp:5433 &

# connect via psql
$ psql "host=127.0.0.1 sslmode=disable dbname=ci_stats user=$USER@gitlab.com port=5433"
```
